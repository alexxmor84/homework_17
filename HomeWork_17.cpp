#include <iostream>

class Vector
{
public:
    Vector() : x(0), y(0), z(0), l(0)
    {}

    Vector(double _x, double _y, double _z, double _l) : x(_x), y(_y), z(_z), l(_l)
    {}

    void Length()
    {
        l = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
    }

    void Show()
    {
        std::cout << l;
    }
private:
    double x;
    double y;
    double z;
    double l;
};

int main()
{
    Vector v(7, 16, 8, 0);
    v.Length();
    v.Show();
}
